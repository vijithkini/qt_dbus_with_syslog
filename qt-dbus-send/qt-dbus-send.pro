QT -= gui
QT += dbus

CONFIG += debug

HEADERS += ping-common.h \
    qsyslog.h
SOURCES += ping.cpp \
    qsyslog.cpp

target.path = /home/claysol/Desktop/qt-dbus-send
INSTALLS += target
