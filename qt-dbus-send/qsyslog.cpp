#include "qsyslog.h"

#include <QTime>
#include <QDebug>

QSyslog* QSyslog::singleton = NULL;
QString QSyslog::appName = "call_setAppName_before_using";

QSyslog::QSyslog() :
QObject(NULL)
{
}

QSyslog& QSyslog::instance()
{
if( NULL == singleton )
{
singleton = new QSyslog();
void openlog(const char *ident, int option, int facility);
}
return *singleton;
}
void QSyslog::free()
{
if( NULL != singleton )
{
delete singleton;
singleton = NULL;
closelog();
}
}

void QSyslog::Syslog(int level, QString message)
{
QTime rightNow = QTime::currentTime();
qDebug() << rightNow.toString() + " " + QString::number(level) + " " + message;
syslog(level, (const char *)message.toLatin1());
}
