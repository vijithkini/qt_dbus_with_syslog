#ifndef QSYSLOG_H
#define QSYSLOG_H

#include <QObject>
#include <syslog.h>

class QSyslog : public QObject
{
Q_OBJECT
public:
static void setAppName(QString appName) { QSyslog::appName = appName; }
static QSyslog& instance(); // singleton accessor
static void free(); // singleton free

static void Syslog(int level, QString message);

private:
QSyslog();

static QSyslog* singleton;
static QString appName;

};
#endif
// QSYSLOG_H
