#ifndef APPUI_H
#define APPUI_H

#include <QtCore/QObject>

class AppUI: public QObject
{
    Q_OBJECT
public slots:
    Q_SCRIPTABLE QString ping(const QString &arg);
};

#endif
